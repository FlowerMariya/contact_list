import { Prop } from '@nestjs/mongoose';

export abstract class BaseSchema {
  @Prop({ default: () => new Date() })
  createdOn: Date;

  @Prop({ default: () => new Date() })
  updatedOn: Date;
}

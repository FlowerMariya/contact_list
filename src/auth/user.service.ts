import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { user } from 'src/utils/login_user';

@Injectable()
export class UserService {
  private users = [{ username: user.username, password: user.password }];

  async findByUsername(username: string) {
    try {
      return this.users.find((user) => user.username === username);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
}

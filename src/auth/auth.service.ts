import { JwtService } from '@nestjs/jwt';
import { UserService } from './user.service';
import {
  Injectable,
  HttpException,
  HttpStatus,
  UnauthorizedException,
} from '@nestjs/common';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  async signIn(username: string, password: string): Promise<string> {
    try {
      const user = await this.userService.findByUsername(username);

      if (!user) {
        throw new UnauthorizedException('Invalid credentials');
      }

      if (password != user.password) {
        throw new UnauthorizedException('Invalid credentials');
      }

      const payload = { username: user.username };

      return this.jwtService.sign(payload);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
}

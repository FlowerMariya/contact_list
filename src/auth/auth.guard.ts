import { Observable } from 'rxjs';
import { JwtService } from '@nestjs/jwt';
import {
  Injectable,
  HttpException,
  CanActivate,
  ExecutionContext,
  HttpStatus,
} from '@nestjs/common';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private readonly jwtService: JwtService) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    try {
      const request = context.switchToHttp().getRequest();

      const jwt = request.headers.authorization?.split(' ')[1];

      const decoded = this.jwtService.verify(jwt, {
        secret: process.env.JWT_SECRET_KEY,
      });

      request.user = decoded;

      if (request.user) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      throw new HttpException('Not authorized', HttpStatus.UNAUTHORIZED);
    }
  }
}

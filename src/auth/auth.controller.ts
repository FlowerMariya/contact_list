import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { HttpException, HttpStatus } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthDto } from './dto/auth.input';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('signIn')
  @ApiOperation({ summary: 'Sign in using username and password to get token' })
  async signIn(@Body() authDto: AuthDto) {
    try {
      const token = await this.authService.signIn(
        authDto.username,
        authDto.password,
      );

      return { token };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
}

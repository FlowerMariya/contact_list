import { Test, TestingModule } from '@nestjs/testing';
import { ContactsController } from './contacts.controller';
import { ContactsService } from './contacts.service';
import { CreateContactDto } from './dto/create-contact.dto';
import { HttpStatus } from '@nestjs/common';
import { ContactsModule } from './contacts.module';
import { MongooseConfigModule } from '../config/mongoose.config';
import { MongooseModule } from '@nestjs/mongoose';
import { Contact, ContactSchema } from '../entities/contact.entity';
import { JwtService } from '@nestjs/jwt';

describe('ContactsController', () => {
  let controller: ContactsController;
  let service: ContactsService;
  let module: TestingModule;

  beforeEach(async () => {
    module = await Test.createTestingModule({
      controllers: [ContactsController],
      providers: [ContactsService, JwtService],
      imports: [
        MongooseConfigModule,
        ContactsModule,
        MongooseModule.forFeature([
          {
            name: Contact.name,
            schema: ContactSchema,
          },
        ]),
      ],
    }).compile();

    controller = module.get<ContactsController>(ContactsController);
    service = module.get<ContactsService>(ContactsService);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('controller should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('service Should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    it('should create a contact', async () => {
      const createContactDto: CreateContactDto = {
        firstName: 'Flower',
        lastName: 'Maria',
        email: 'flowermariya@gmail.com',
        phone: '9874563210',
        address: 'House',
        city: 'Thrips',
        state: 'Kerala',
        country: 'India',
        zipCode: '680012',
      };

      const output = {
        status: HttpStatus.OK,
        message: 'RESPONSE_MESSAGES.SUCCESS',
        result: {
          firstName: 'Flower',
          lastName: 'Maria',
          email: 'flowermariya@gmail.com',
          phone: '9874563210',
          address: 'House',
          city: 'Thrips',
          state: 'Kerala',
          country: 'India',
          zipCode: '680012',
        },
      };

      jest.spyOn(service, 'create').mockImplementation(() => output as any);

      expect(await controller.create(createContactDto)).toEqual(
        expect.objectContaining(output),
      );
    });
  });
});

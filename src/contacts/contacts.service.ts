import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { CreateContactDto } from './dto/create-contact.dto';
import { UpdateContactDto } from './dto/update-contact.dto';
import { Contact } from '../entities/contact.entity';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
// import { RESPONSE_MESSAGES } from 'src/utils/response.constants';

@Injectable()
export class ContactsService {
  constructor(
    @InjectModel(Contact.name) private contactModel: Model<Contact>,
  ) {}

  async create(createContactDto: CreateContactDto) {
    try {
      const newContact = await this.contactModel.create({
        ...createContactDto,
      });

      const savedContact = await newContact.save();

      return {
        status: HttpStatus.OK,
        message: 'RESPONSE_MESSAGES.SUCCESS',
        result: savedContact,
      };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }

  async findAll() {
    try {
      const allContacts = await this.contactModel.find();

      return {
        status: HttpStatus.OK,
        message: 'RESPONSE_MESSAGES.SUCCESS',
        result: allContacts,
      };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }

  async findOne(id: string) {
    try {
      const result = await this.contactModel.findOne({ _id: id });

      if (!result) {
        throw new HttpException(
          `Contact with ${id} not found`,
          HttpStatus.NOT_FOUND,
        );
      }

      return {
        status: HttpStatus.OK,
        message: 'RESPONSE_MESSAGES.SUCCESS',
        result,
      };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }

  async update(id: string, updateContactDto: UpdateContactDto) {
    try {
      if (Object.keys(updateContactDto).length === 0) {
        throw new Error('No data found to update');
      }

      const existingContact = await this.findOne(id);

      if (!existingContact) {
        throw new HttpException(
          `Contact with ${id} not found to update`,
          HttpStatus.NOT_FOUND,
        );
      }

      const result = await this.contactModel.findByIdAndUpdate(
        { _id: id },
        updateContactDto,
        { new: true },
      );

      return {
        status: HttpStatus.OK,
        message: 'RESPONSE_MESSAGES.SUCCESS',
        result,
      };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }

  async remove(id: string) {
    try {
      const existingContact = await this.findOne(id);

      if (!existingContact) {
        throw new HttpException(
          `Contact with ${id} not found to delete`,
          HttpStatus.NOT_FOUND,
        );
      }

      const result = await this.contactModel.findByIdAndDelete({ _id: id });

      return {
        status: HttpStatus.OK,
        message: 'RESPONSE_MESSAGES.SUCCESS',
        result,
      };
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }
}

import { Test, TestingModule } from '@nestjs/testing';
import { ContactsService } from './contacts.service';

describe('ContactsService', () => {
  let service: ContactsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: ContactsService,
          useValue: {
            create: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<ContactsService>(ContactsService);
  });

  it('should create a new contact', async () => {
    const createContactDto = {
      firstName: 'Flower',
      lastName: 'Maria',
      email: 'flowermariya',
      phone: '9874563210',
      address: 'House',
      city: 'Thrips',
      state: 'Kerala',
      country: 'India',
      zipCode: '680012',
    };

    jest.spyOn(service, 'create').mockResolvedValue({
      status: 200,
      message: 'RESPONSE_MESSAGES.SUCCESS',
      result: createContactDto as any,
    });

    const response = await service.create(createContactDto);

    expect(response).toEqual({
      status: 200,
      message: 'RESPONSE_MESSAGES.SUCCESS',
      result: createContactDto,
    });
  });
});

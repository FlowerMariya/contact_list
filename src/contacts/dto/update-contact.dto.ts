import {
  IsString,
  IsEmail,
  IsOptional,
  IsNotEmpty,
  MaxLength,
  MinLength,
} from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class UpdateContactDto {
  @ApiPropertyOptional({ description: 'First name of the contact' })
  @IsString()
  @MinLength(1)
  @MaxLength(20)
  @IsOptional({ message: 'firstName cant be empty' })
  firstName: string;

  @ApiPropertyOptional({ description: 'Last name of the contact' })
  @IsString()
  @MinLength(1)
  @MaxLength(20)
  @IsOptional()
  lastName: string;

  @ApiPropertyOptional({ description: 'Email address of the contact' })
  @IsEmail()
  @IsOptional()
  email: string;

  @ApiPropertyOptional({ description: 'Phone number of the contact' })
  @IsString()
  @IsOptional()
  phone: string;

  @ApiPropertyOptional({ description: 'Address of the contact' })
  @IsString()
  @MinLength(3)
  @MaxLength(50)
  @IsOptional()
  address: String;

  @ApiPropertyOptional({ description: 'City of the contact' })
  @IsString()
  @IsOptional()
  city: String;

  @ApiPropertyOptional({ description: 'State of the contact' })
  @IsString()
  @IsOptional()
  state: String;

  @ApiPropertyOptional({ description: 'Country of the contact' })
  @IsString()
  @IsOptional()
  country: String;

  @ApiPropertyOptional({ description: 'ZIP code of the contact' })
  @IsString()
  @IsOptional()
  zipCode: String;
}
